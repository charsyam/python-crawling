#!/bin/bash

cd /etc/apt/
sed 's/kr.archive.ubuntu.com/ftp.daumkakao.com/g' sources.list | sudo tee sources.list

cd 
sudo apt-get update
sudo apt-get install git curl feh openjdk-8-jdk python3-dev python3-pip libfreetype6-dev libpng12-dev pkg-config libffi-dev

pip3 install --upgrade pip
wget http://mirrors.jenkins.io/war-stable/latest/jenkins.war

cd python-crawling
pip3 install -r ./requirements.txt --user
