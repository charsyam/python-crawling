from bs4 import BeautifulSoup
import re
import requests 
import pprint 
import lxml.etree
import lxml.html


URL="http://finance.daum.net/exchange/exchangeMain.daum"

def create_header():
    return {
        "User-agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36", 
    }

def get_exchanges():
    exchanges = []
    r = requests.get(URL,headers=create_header()) 
    contents = BeautifulSoup(r.content,"html.parser")
    div_values = contents.find_all("div", class_="exDetailBox")
    for div in div_values:
        atag = div.find("a")
        href = atag["href"]
        code=re.findall('[^=]*$', href)[0]
        name = atag.get_text()
        price = div.find("dd", class_='exPrice').get_text()
        exchanges.append({
            'name': name,
            'price': price,
            'code': code
        })
        

    return exchanges


if __name__ == '__main__':
    exchanges = get_exchanges()
    for exchange in exchanges:
        pprint.pprint(exchange)
