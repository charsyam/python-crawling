from bs4 import BeautifulSoup 
from konlpy.tag import Kkma
from konlpy.utils import pprint
import requests 
import re 
import sys 
import pprint 
import hashlib
import collections
import json

# 네이버 뉴스 url을 입력합니다.
SIDS = [
    100, #정치
    101, #경제
    102, #사회
    103, #생활
    104, #세계
    105  #IT
]

NEWS_HOST="https://m.news.naver.com"
URL="{host}/main.nhn?mode=LSD&sid1={sid}"
READ_PAGE_MARK="/read.nhn?"


def create_header():
    return {
        "User-agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36", 
        "referer":"https://m.news.naver.com/", 
    }

def get_news_url(link):
    return "{host}{link}".format(host=NEWS_HOST, link=link)

def get_news_contents(link):
    url = get_news_url(link)
    r = requests.get(url,headers=create_header()) 

    root = BeautifulSoup(r.content,"html.parser")     
    tag = root.find("div", class_="newsct_article _news_article_body")
    texts = tag.find_all(text=True)
    text = [t for t in texts]
    return '\n'.join(text).strip()

def get_section_news(sid, store):
    if store == None:
        store = []

    url = URL.format(host=NEWS_HOST, sid=sid)
    r = requests.get(url,headers=create_header()) 

    root = BeautifulSoup(r.content,"html.parser")     
    links = root.find_all("a")
    for link in links:
        if link["href"].startswith(READ_PAGE_MARK):
            n = {
                "link" : link["href"],
                "subject" : link.get_text().strip() 
            }

            if len(n["subject"]) > 0:
                store.append(n)

    return store


if __name__ == '__main__':
    cnt = collections.Counter()
    nouns = collections.Counter()
    kkma = Kkma()
    
    news = []       
    for sid in SIDS:
        news = get_section_news(sid, news)

    f = open('urls.txt', 'w')
    for n in news:
        url = get_news_url(n["link"])
        f.write(url+"\r\n")
    f.close()
