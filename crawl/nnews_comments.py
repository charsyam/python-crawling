from bs4 import BeautifulSoup 
from konlpy.tag import Kkma
from konlpy.utils import pprint
import requests 
import re 
import sys 
import pprint 
import hashlib
import collections
import json
import pytagcloud


# 네이버 뉴스 url을 입력합니다.
urls = [
    "https://news.naver.com/main/read.nhn?mode=LSD&mid=shm&sid1=100&oid=001&aid=0010250394",
    "https://news.naver.com/main/read.nhn?mode=LSD&mid=shm&sid1=100&oid=047&aid=0002198507"
]


def get_oid_and_aid(url):
    oid=url.split("oid=")[1].split("&")[0] 
    aid=url.split("aid=")[1] 
    return oid, aid

def create_header(url):
    return {
        "User-agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36", 
        "referer":url, 
    }

def get_comments(url, oid, aid, cnt=None):
    page = 1

    comments = []

    while True : 
        c_url="https://apis.naver.com/commentBox/cbox/web_naver_list_jsonp.json?ticket=news&templateId=default_society&pool=cbox5&_callback=jQuery1707138182064460843_1523512042464&lang=ko&country=&objectId=news"+oid+"%2C"+aid+"&categoryId=&pageSize=20&indexSize=10&groupId=&listType=OBJECT&pageType=more&page="+str(page)+"&refresh=false&sort=FAVORITE"  

        # 파싱하는 단계입니다.
        r = requests.get(c_url,headers=create_header(url)) 
        contents = BeautifulSoup(r.content,"html.parser")     
        contents = str(contents)[40:-2]

        comment_json = json.loads(contents)
        comment_list = comment_json['result']['commentList']

        for comment in comment_list:
            v = {'news': comment['objectId'],
                 'contents': comment['contents'],
                 'user_id': comment['userIdNo'],
                 'user_name': comment['userName']}
                 
            comments.append(v)
            if cnt:
                cnt[v['user_id']] += 1

        #comments.append(match) 
        if len(comments) < ((page) * 20): 
            break 
        else :  
            page+=1

    return comments


#cnt = collections.Counter()
#for l in flatten(List):
#    c = hashlib.md5(l.encode('utf-8')).hexdigest()
#    cnt[c] += 1

#for k in cnt:
#    print(k, cnt[k])

if __name__ == '__main__':
    cnt = collections.Counter()
    nouns = collections.Counter()
    kkma = Kkma()
    for url in urls:
        oid, aid = get_oid_and_aid(url)
        comments = get_comments(url, oid, aid, cnt=cnt)
        for comment in comments:
            for noun in kkma.nouns(comment['contents']):
                nouns[noun] += 1

    tags = nouns.most_common(100)
    tag_list = pytagcloud.make_tags(tags, maxsize=80)
    pytagcloud.create_tag_image(tag_list, 'wordcloud.jpg', size=(900, 600), fontname='Nanum Gothic Coding', rectangular=False)

