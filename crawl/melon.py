from bs4 import BeautifulSoup
import requests
import json

def create_header(referer):
    return {
        "User-agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
        "referer": referer,
    }

#https://www.melon.com/chart/#params%5Bidx%5D=1
idx = [
    (1, 50),
    (51, 100)
]

def create_url(idx):
    return "https://www.melon.com/chart/#params%5Bidx%5D={}".format(idx)

def get_links(referer, ids):
    url = "https://www.melon.com/commonlike/getSongLike.json?contsIds="
    str_ids = ','.join([str(i) for i in ids])
    url = url + str_ids
    
    r = requests.get(url, headers=create_header(referer))
    #{"contsLike":[{"CONTSID":31151836,"LIKEYN":"N","SUMMCNT":169670},{"CONTSID":31230093,"LIKEYN":"N","SUMMCNT":84766},{"CONTSID":31175119,"LIKEYN":"N","SUMMCNT":105965},{"CONTSID":31151836,"LIKEYN":"N","SUMMCNT":169670},{"CONTSID":31230093,"LIKEYN":"N","SUMMCNT":84766},{"CONTSID":31175119,"LIKEYN":"N","SUMMCNT":105965}],"httpDomain":"http://www.melon.com","httpsDomain":"https://www.melon.com","staticDomain":"https://static.melon.co.kr"}
    #
    #
    likes = json.loads(r.content.decode('utf-8'))
    return likes['contsLike']

def get_melon_list(html, tid, order_init):
    root = BeautifulSoup(html,"html.parser")
    #<tr class="lst50"
    orders = {}
    order = order_init
    tag_class = "lst{}".format(tid)
    songs = root.find_all("tr", class_=tag_class)
    song_ids = []
    for song in songs:
        #<div class="ellipsis rank01">
        songid = int(song["data-song-no"])
        name = song.find("div", class_="ellipsis rank01").get_text().strip()
        singer = song.find("div", class_="ellipsis rank02").get_text().strip()
        album = song.find("div", class_="ellipsis rank03").get_text().strip()
        orders[songid] = {'id': songid, 'name': name, 'singer': singer, 'album': album, 'order': order}
        song_ids.append(songid)
        order += 1
        
    likes = get_links('https://www.melon.com/chart/index.htm', song_ids)
    
    for like in likes:
        sid = like['CONTSID']
        orders[sid]['likes'] = like['SUMMCNT']
        
    return orders

items = []
for i in idx:
    url = create_url(i[0])
    r = requests.get(url, headers=create_header("https://www.melon.com/chart/index.htm"))
    l = get_melon_list(r.content, i[1], i[0])
    for item in l:
        items.append(l[item])

items = sorted(items, key=lambda item: item['order'])

for item in items:
    print(item)

import pandas as pd
import numpy as np
from datetime import datetime
import csv
import sys
import matplotlib.pyplot as plt
import matplotlib.dates as md
import pandas.plotting._converter as pandacnv

dfItem = pd.DataFrame.from_records(items)

x = dfItem["order"]
y = dfItem["likes"]

plt.plot(x,y, linestyle=':')
plt.show()
