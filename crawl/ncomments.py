from bs4 import BeautifulSoup 
from konlpy.tag import Kkma
from konlpy.utils import pprint
import requests 
import re 
import sys 
import pprint 
import hashlib
import collections
import json
import redis

rconn = redis.StrictRedis()


def get_oid_and_aid(url):
    oid=url.split("oid=")[1].split("&")[0].strip() 
    aid=url.split("aid=")[1].split("&")[0].strip()
    return oid, aid

def create_header(url):
    return {
        "User-agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36", 
        "referer":url, 
    }

def get_comments(url, oid, aid):
    page = 1

    comments = []

    while True : 
        c_url="https://apis.naver.com/commentBox/cbox/web_naver_list_jsonp.json?ticket=news&templateId=default_society&pool=cbox5&_callback=jQuery1707138182064460843_1523512042464&lang=ko&country=&objectId=news"+oid+"%2C"+aid+"&categoryId=&pageSize=20&indexSize=10&groupId=&listType=OBJECT&pageType=more&page="+str(page)+"&refresh=false&sort=FAVORITE"  

        # 파싱하는 단계입니다.
        r = requests.get(c_url,headers=create_header(url)) 
        contents = BeautifulSoup(r.content,"html.parser")     
        contents = str(contents)[40:-2]

        comment_json = json.loads(contents)
        comment_list = comment_json['result']['commentList']

        for comment in comment_list:
            v = {'news': comment['objectId'],
                 'contents': comment['contents'],
                 'user_id': comment['userIdNo'],
                 'user_name': comment['userName'],
                 'cid': comment['commentNo'],
                 'time': comment['modTime']}
                 
            comments.append(v)

        #comments.append(match) 
        if len(comments) < ((page) * 20): 
            break 
        else :  
            page+=1

    return comments


RANK_KEY = "nc:crank"
RANK_USER_KEY = "nc:user"
DUPL_KEY = "nc:dupl"
COMM_KEY = "cc:"
COMM_INFO_KEY = "ci:"
COMM_USER_KEY = "cu:"

if __name__ == '__main__':
    filename = sys.argv[1]
    urls = open(filename).readlines()
    for url in urls:
        url = url.strip()
        oid, aid = get_oid_and_aid(url)
        dupl = "{}:{}".format(oid, aid)
        if rconn.sismember(DUPL_KEY, dupl):
            continue

        comments = get_comments(url, oid, aid)
        for comment in comments:
            h = hashlib.md5(comment['contents'].encode('utf-8')).hexdigest()
            user_id = comment['user_id']
            user_name = comment['user_name']
            cid = str(comment['cid'])
            time = str(comment['time'])

            info = url + ":" + user_id + ":" + user_name + ':' + cid + ':' + time
            p = rconn.pipeline(transaction=False)
            p.zincrby(RANK_KEY, h, 1)
            p.sadd(COMM_INFO_KEY + h, info)
            p.sadd(COMM_USER_KEY + user_id, h + ':' + cid + ':' + time)
            p.set(COMM_KEY + h, comment['contents'])
            p.zincrby(RANK_USER_KEY, user_id, 1)
            p.execute()
