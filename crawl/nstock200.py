from bs4 import BeautifulSoup 
from konlpy.tag import Kkma
from konlpy.utils import pprint
import requests 
import re 
import sys 
import pprint 
import hashlib
import collections
import json
from naver_stock_html_parser import NaverStockHtmlParser


URL="https://finance.naver.com/sise/entryJongmok.nhn?&page={page}"
PAGE_MAX=21

def create_header():
    return {
        "User-agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36", 
        "referer": "https://finance.naver.com/sise/sise_index.nhn?code=KPI200", 
    }

def get_stocks(page):
    url = URL.format(page=page)
    r = requests.get(url,headers=create_header()) 
    parser = NaverStockHtmlParser()
    parser.feed(r.content.decode('euc-kr'))
    return parser.stocks


if __name__ == '__main__':
    kkma = Kkma()
    for page in range(1, PAGE_MAX+1):
        stocks = get_stocks(page)
        for stock in stocks:
            print(stock)
