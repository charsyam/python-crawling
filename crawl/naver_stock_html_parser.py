from html.parser import HTMLParser

class NaverStockHtmlParser(HTMLParser):
    def __init__(self):
        super(NaverStockHtmlParser, self).__init__()
        self.start_stock = False
        self.name_flag = False
        self.price_flag = False
        self.number_2_cnt = 0
        self.tmp_data = {}
        self.stocks = []

    def handle_starttag(self, tag, attrs):
        if tag == "td":
            for attr in attrs:
                if not self.start_stock and attr[0] == 'class' and attr[1] == 'ctg':
                    self.start_stock = True
                    self.name_flag = True
                    break
                if self.start_stock and attr[0] == 'class' and attr[1] == "number_2":
                    self.number_2_cnt += 1
                    if self.number_2_cnt == 1:
                        self.price_flag = True

    def handle_endtag(self, tag):
        if tag == "tr":
            self.start_stock = False
            self.number_2_cnt = 0
            self.name_flag = False
            self.price_flag = False

            if len(self.tmp_data) > 0:
                self.stocks.append(self.tmp_data)

            self.tmp_data = {}

    def handle_data(self, data):
        if self.start_stock:
            if self.name_flag:
                self.tmp_data["name"] = data
                self.name_flag = False
            elif self.price_flag:
                self.tmp_data["price"] = data
                self.price_flag = False
