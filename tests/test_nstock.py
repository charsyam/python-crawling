from crawl.naver_stock_html_parser import NaverStockHtmlParser

def test_page_parsing():
    f = open('./tests/test_stock_page1.html', encoding='euc-kr')
    html = f.read()
    parser = NaverStockHtmlParser()
    parser.feed(html)
