from crawl.add import add

def test_add():
	assert add(1,2) == 3

def test_add2():
	assert add(2,2) == 4

def test_add3():
	assert add(3,3) == 6
